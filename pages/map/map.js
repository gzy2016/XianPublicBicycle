Page({

  /**
   * 页面的初始数据
   */
  data: {
    longitude:'113.324520',
    latitude:'23.099994',
    scale:'14',
    markers:[]
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this = this;
    wx.getLocation({
      success: function(res) {
        _this.setData({
          longitude: res.longitude,
          latitude: res.latitude
        });
        // 更新缓存信息
        _this.synchronizeData();

        // 获取当前位置信息站点信息
        _this.getLocationInfo();
      },
    });

  },

  synchronizeData:function(){
    var url = "https://www.bkybk.com/bike/synchronize?t=" + new Date().getTime();
    wx.request({
      url: url,
      method: 'post',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
      }
    });
  },

  getLocationInfo:function(){
    var _this = this;
    var markers = new Array();
    wx.request({
      url: 'https://www.bkybk.com/bike/getSearch',
      data: {
        lng: this.data.longitude,
        lat: this.data.latitude,
        radius: "1500"
      },
      method:'get',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success:function(res){
        if (200 == res.statusCode && res.data){
          for(var i = 0; i < res.data.length; i++){
            var marker = {
              id: res.data[i].siteid,
              longitude: res.data[i].longitude,
              latitude: res.data[i].latitude,
              iconPath: res.data[i].siteState==1?'../images/zxc_g1.png':'../images/zxc_r1.png'
            };
            markers.push(marker);
          }
          
        }
      },
      complete: function(){
        _this.setData({
          markers: markers
        });
      }
    })

    wx.hideShareMenu();
  },

  markertap:function(e) {
    wx.navigateTo({
      url: '/pages/map/site-detail/site-detail?id=' + e.markerId
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})