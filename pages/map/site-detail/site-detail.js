Page({

  /**
   * 页面的初始数据
   */
  data: {
    siteInfo:{
      siteid:'正在获取信息…… ',
      sitename:'正在获取信息……',
      location:'正在获取信息……',
      emptynum:'正在获取信息……',
      updateTime:'正在获取信息……',
      stautsText:'正在获取信息……',
      canRented:'正在获取信息……',
      iconType:'info',
      iconColor:'green'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var _this = this;
    var url = "https://www.bkybk.com/bike/get/site/" + id;
    wx.request({
      url: url,
      success: function (res) {
         if (200 == res.statusCode && res.data) {
           var obj = res.data;
           if (obj.siteState == '1'){
             obj.iconType = 'info';
             obj.iconColor = 'green';
             obj.stautsText = '站点正常';
           }else{
             obj.iconType = 'warn';
             obj.iconColor = 'red';
             obj.stautsText = '站点异常';
           }
           obj.canRented = (obj.locknum - obj.emptynum);
           _this.setData({
             siteInfo: obj
           });
         }else{
           // 获取数据失败
         }
         

      }
    });
    wx.hideShareMenu();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})